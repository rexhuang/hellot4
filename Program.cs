﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//使用 T4 文字範本在設計階段產生程式碼
//https://msdn.microsoft.com/zh-tw/library/dd820620.aspx

namespace HelloT4
{
    class Program
    {


        static void Main(string[] args)
        {
         　String inputFile = @"C:\HelloT4\INPUT.txt";
           String outputPath = @"C:\HelloT4\txtoutput\";

            System.IO.StreamReader file =
                new System.IO.StreamReader(inputFile);

            int counter = 0;
            string line;
            while ((line = file.ReadLine()) != null)
            {
                //寫法一:無法傳入參數
                //CBxxxxService t1 = new CBxxxxService();
                //string result = t1.TransformText();

                //寫法二:可以傳入參數(把line傳給tt檔的tempClassId)
                string result = new CBxxxxService
                {
                    tempClassId = line,
                }.TransformText();
                

                File.WriteAllText(outputPath + line + "Service.cs", result);
                counter++;
            }

            file.Close();
            Console.WriteLine("執行完畢!");
            Console.ReadLine();
        }

    }
}

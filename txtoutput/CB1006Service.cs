  

/*************************************************************************************
功能名稱：(CBxxxx)進貨明細表
功能描述：

版權所有：某某公司
程式撰寫： 
撰寫日期：2017/5/1

改版日期：
改版備註：
**************************************************************************************/
using System;
using System.Data;
using System.Data.SqlClient;
using log4net;
//記得要引用
using Abc.Stock.DAOImpl;
using Abc.Stock.DTO;


namespace Abc.Stock.Service
{
    /// <summary>
    /// 服務
    /// (CBxxxx)進貨明細表
    /// 作者 : 
    /// </summary>
    public class CB1234Service : MarshalByRefObject
    {
        #region 自訂參數區
        /// <summary>
        /// 資料庫連線
        /// </summary>
        private SqlConnection cn = null;
        private SqlTransaction trans = null;
        
        //預設的Logger(log4net)
        private ILog log = LogManager.GetLogger(typeof(CB1234Service));


        #endregion

        public CB1234Service()
            : base()
        {

            SetLogger(typeof(CB1234Service));
        }


        /*******************************************************************************************
      * 設定Logger類別。      
      *******************************************************************************************/
        protected void SetLogger(System.Type classType)
        {
            log = LogManager.GetLogger(classType);
        }

        /*******************************************************************************************
        * 設定Logger類別。
        * 讓log可以正確顯示訊息出處。
        *******************************************************************************************/
        protected void ExplainException(Exception e)
        {
            try
            {
                throw e;
            }
            catch (SqlException se)
            {
                if (log.IsDebugEnabled)
                {
                    log.Debug(se.StackTrace);
                }
                else
                {
                    throw se;
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error(ex.StackTrace);
                }
                throw ex;
            }

        }
        #region 查詢
        public DataTable SelectDefaultData(string inputSchema)
        {
            cn = new ConnectionSqlFactory().GetSqlConnection();
            //修改引用物件
            CB1234DAOImpl cb1234dao = new CB1234DAOImpl(cn, null, inputSchema);
            try
            {
                //修改引用方法
                return cb1234dao.SelectDefaultData();                
            }
            catch (Exception ex)
            {
                log.Debug(ex.Message);
                throw new ServiceException(this.GetType().Name, ex.Message);
            }
        }
        #endregion


        #region 新增\刪除\修改
        public int InsertData(CB1234DTO dtoObj, string inputSchema)
        {
            int addnum = 0;
            cn = new ConnectionSqlFactory().GetSqlConnection();
            trans = cn.BeginTransaction();
            //修改引用物件
            CB1234DAOImpl cb1234dao = new CB1234DAOImpl(cn, trans, inputSchema);
            try
            {
                //修改引用方法
                addnum = cb1234dao.InsertData(dtoObj);
                trans.Commit();
                cn.Close();
                return addnum;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                log.Debug(ex.Message);
                throw new ServiceException(this.GetType().Name, ex.Message);
            }
        }

        public int DeleteAllVersionData(CB1234DTO dtoObj, string inputSchema)
        {
            int delnum = 0;
            cn = new ConnectionSqlFactory().GetSqlConnection();
            trans = cn.BeginTransaction();
            //修改引用物件
            CB1234DAOImpl cb1234dao = new CB1234DAOImpl(cn, trans, inputSchema);
            try
            {
                //修改引用方法
                delnum = cb1234dao.DeleteAllVersionData(dtoObj);
                trans.Commit();
                cn.Close();
                return delnum;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                log.Debug(ex.Message);
                throw new ServiceException(this.GetType().Name, ex.Message);
            }
        }


        public int DeleteData(CB1234DTO dtoObj, string inputSchema)
        {
            int delnum = 0;
            cn = new ConnectionSqlFactory().GetSqlConnection();
            trans = cn.BeginTransaction();
            //修改引用物件
            CB1234DAOImpl cb1234dao = new CB1234DAOImpl(cn, trans, inputSchema);
            try
            {
                //修改引用方法
                delnum = cb1234dao.DeleteData(dtoObj);
                trans.Commit();
                cn.Close();
                return delnum;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                log.Debug(ex.Message);
                throw new ServiceException(this.GetType().Name, ex.Message);
            }
        }


        public int UpdateVersionToUsed(CB1234DTO dtoObj, string inputSchema)
        {
            int updatenum = 0;
            cn = new ConnectionSqlFactory().GetSqlConnection();
            trans = cn.BeginTransaction();
            //修改引用物件
            CB1234DAOImpl cb1234dao = new CB1234DAOImpl(cn, trans, inputSchema);
            try
            {
                //修改引用方法
                updatenum = cb1234dao.UpdateVersionToUsed(dtoObj);
                trans.Commit();
                cn.Close();
                return updatenum;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                log.Debug(ex.Message);
                throw new ServiceException(this.GetType().Name, ex.Message);
            }
        }

        public int UpdateVersionToNotUsed(CB1234DTO dtoObj, string inputSchema)
        {
            int updatenum = 0;
            cn = new ConnectionSqlFactory().GetSqlConnection();
            trans = cn.BeginTransaction();
            //修改引用物件
            CB1234DAOImpl cb1234dao = new CB1234DAOImpl(cn, trans, inputSchema);
            try
            {
                //修改引用方法
                updatenum = cb1234dao.UpdateVersionToNotUsed(dtoObj);
                trans.Commit();
                cn.Close();
                return updatenum;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                log.Debug(ex.Message);
                throw new ServiceException(this.GetType().Name, ex.Message);
            }
        }

        public int UpdateData(CB1234DTO dtoObj, string inputSchema)
        {
            int updatenum = 0;
            cn = new ConnectionSqlFactory().GetSqlConnection();
            trans = cn.BeginTransaction();
            //修改引用物件
            CB1234DAOImpl cb1234dao = new CB1234DAOImpl(cn, trans, inputSchema);
            try
            {
                //修改引用方法
                updatenum = cb1234dao.UpdateData(dtoObj);
                trans.Commit();
                cn.Close();
                return updatenum;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                log.Debug(ex.Message);
                throw new ServiceException(this.GetType().Name, ex.Message);
            }
        }
        #endregion
    }
}
